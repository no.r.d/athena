{
    "extensions": {
        "athena": {
            "locations": {
                "code": [
                    "null://.",
                    "dir:///Users/nate/Desktop/Dev/NoRD/athena/src/nord/athena/",
                    "dir:///Users/nate/Desktop/Dev/NoRD/athena/src/nord"
                ],
                "files": [
                    "dir:///Users/nate/.virtualenvs/nord_run/lib/python3.7/site-packages/nord/athena",
                    "dir:///Users/nate/Desktop/Dev/NoRD/athena/src/nord/athena/",
                    "dir:///Users/nate/Desktop/Dev/NoRD/athena/src/nord"
                ],
                "path": [
                    "dir:///Users/nate/.virtualenvs/nord_run/lib/python3.7/site-packages/nord/athena",
                    "dir:///Users/nate/Desktop/Dev/NoRD/athena/src/nord/athena/",
                    "dir:///Users/nate/Desktop/Dev/NoRD/athena/src/nord"
                ]
            }
        },
        "fregata": {
            "locations": {
                "code": [
                    "null://."
                ],
                "files": [
                    "dir://../athena/tests/maps",
                    "dir:///Users/nate/.virtualenvs/nord_run/lib/python3.7/site-packages/nord/fregata",
                    "dir://."
                ],
                "path": [
                    "dir://../athena/tests/maps",
                    "dir:///Users/nate/.virtualenvs/nord_run/lib/python3.7/site-packages/nord/fregata",
                    "dir://."
                ]
            }
        },
        "pasion": {
            "locations": {
                "code": [
                    "null://."
                ],
                "files": [
                    "dir:///Users/nate/.virtualenvs/nord_run/lib/python3.7/site-packages/nord/pasion"
                ],
                "path": [
                    "dir:///Users/nate/.virtualenvs/nord_run/lib/python3.7/site-packages/nord/pasion"
                ]
            }
        },
        "wrigan": {
            "locations": {
                "code": [
                    "null://."
                ],
                "files": [
                    "dir:///Users/nate/.virtualenvs/nord_run/lib/python3.7/site-packages/nord/wrigan"
                ],
                "path": [
                    "dir:///Users/nate/.virtualenvs/nord_run/lib/python3.7/site-packages/nord/wrigan"
                ]
            }
        }
    },
    "map": {
        "edges": [
            {
                "from_space": "athena",
                "rule": "attach",
                "source": "ATTACHED_BTN:Listener: Attached Button",
                "space": "athena",
                "target": "BALL:Location: Ball",
                "to_space": "athena"
            },
            {
                "from_space": "athena",
                "rule": "move",
                "source": "BALL:Location: Ball",
                "space": "athena",
                "target": "LEFT_LOC:Location: Left Side",
                "to_space": "athena"
            },
            {
                "from_space": "athena",
                "rule": "move",
                "source": "BALL:Location: Ball",
                "space": "athena",
                "target": "RIGHT_LOC:Location: Right Side",
                "to_space": "athena"
            },
            {
                "delegate": {
                    "direction": "as_src",
                    "rule_type": "flow",
                    "uid": "on_click_0"
                },
                "from_space": "athena",
                "rule": "flow",
                "source": "LEFT_BTN:Listener: Left Button",
                "space": "nord",
                "target": "LEFT_LOC:Location: Left Side",
                "to_space": "athena"
            },
            {
                "delegate": {
                    "direction": "as_src",
                    "rule_type": "flow",
                    "uid": "on_click_0"
                },
                "from_space": "athena",
                "rule": "flow",
                "source": "RIGHT_BTN:Listener: Right Button",
                "space": "nord",
                "target": "RIGHT_LOC:Location: Right Side",
                "to_space": "athena"
            }
        ],
        "nodes": [
            {
                "id": "ATTACHED_BTN",
                "name": "Listener: Attached Button",
                "position": [
                    -0.06560724228620529,
                    0.6384162902832031,
                    4.147125720977783
                ],
                "space": "athena",
                "type": "listener"
            },
            {
                "id": "BALL",
                "name": "Location: Ball",
                "position": [
                    0.40051549673080444,
                    3.680980682373047,
                    -2.7966954708099365
                ],
                "space": "athena",
                "type": "location"
            },
            {
                "id": "LEFT_BTN",
                "name": "Listener: Left Button",
                "position": [
                    -4.048696994781494,
                    3.9685630798339844,
                    -8.016712188720703
                ],
                "space": "athena",
                "type": "listener"
            },
            {
                "id": "LEFT_LOC",
                "name": "Location: Left Side",
                "position": [
                    -15.903207778930664,
                    -0.3443412780761719,
                    -2.599259614944458
                ],
                "space": "athena",
                "type": "location"
            },
            {
                "id": "RIGHT_BTN",
                "name": "Listener: Right Button",
                "position": [
                    5.401576995849609,
                    2.5194091796875,
                    -7.768620491027832
                ],
                "space": "athena",
                "type": "listener"
            },
            {
                "id": "RIGHT_LOC",
                "name": "Location: Right Side",
                "position": [
                    16.898895263671875,
                    -0.031982421875,
                    -2.3993265628814697
                ],
                "space": "athena",
                "type": "location"
            }
        ]
    },
    "name": "Arrid Culinary Slobber"
}