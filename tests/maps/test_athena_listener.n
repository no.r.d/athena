{
    "extensions": {
        "athena": {
            "locations": {
                "code": [
                    "null://.",
                    "dir:///Users/nate/Desktop/Dev/NoRD/athena/src/nord/athena/",
                    "dir:///Users/nate/Desktop/Dev/NoRD/athena/src/nord"
                ],
                "files": [
                    "dir:///Users/nate/Desktop/Dev/NoRD/athena/src/nord/athena",
                    "dir:///Users/nate/Desktop/Dev/NoRD/athena/src/nord/athena/",
                    "dir:///Users/nate/Desktop/Dev/NoRD/athena/src/nord"
                ],
                "path": [
                    "dir:///Users/nate/Desktop/Dev/NoRD/athena/src/nord/athena",
                    "dir:///Users/nate/Desktop/Dev/NoRD/athena/src/nord/athena/",
                    "dir:///Users/nate/Desktop/Dev/NoRD/athena/src/nord"
                ]
            }
        },
        "fregata": {
            "locations": {
                "code": [
                    "null://."
                ],
                "files": [
                    "dir://..",
                    "dir:///Users/nate/Desktop/Dev/NoRD/fregata/src/nord/fregata"
                ],
                "path": [
                    "dir://..",
                    "dir:///Users/nate/Desktop/Dev/NoRD/fregata/src/nord/fregata"
                ]
            }
        },
        "pasion": {
            "locations": {
                "code": [
                    "null://."
                ],
                "files": [
                    "dir:///Users/nate/Desktop/Dev/NoRD/pasion/src/nord/pasion"
                ],
                "path": [
                    "dir:///Users/nate/Desktop/Dev/NoRD/pasion/src/nord/pasion"
                ]
            }
        },
        "wrigan": {
            "locations": {
                "code": [
                    "null://."
                ],
                "files": [
                    "dir:///Users/nate/Desktop/Dev/NoRD/wrigan/src/nord/wrigan"
                ],
                "path": [
                    "dir:///Users/nate/Desktop/Dev/NoRD/wrigan/src/nord/wrigan"
                ]
            }
        }
    },
    "map": {
        "edges": [
            {
                "delegate": {
                    "direction": "as_src",
                    "rule_type": "flow",
                    "uid": "on_click_0"
                },
                "from_space": "athena",
                "rule": "flow",
                "source": "981fb275-5096-4ca8-a10c-829ad293cd88:First Click Button",
                "space": "nord",
                "target": "f8bafdf4-6f20-4e94-a622-c24596931e34:First Click Value 'CLICK'",
                "to_space": "nord"
            },
            {
                "from_space": "nord",
                "rule": "assign",
                "source": "fa866847-3d2f-40cd-8475-f6b1bd09c98c:Initial Value 'START'",
                "space": "nord",
                "target": "d086c859-2e2f-4cc7-a9d0-f478706d50b0:Text: Hard Nordic Ship",
                "to_space": "athena"
            },
            {
                "from_space": "nord",
                "rule": "assign",
                "source": "f8bafdf4-6f20-4e94-a622-c24596931e34:First Click Value 'CLICK'",
                "space": "nord",
                "target": "d086c859-2e2f-4cc7-a9d0-f478706d50b0:Text: Hard Nordic Ship",
                "to_space": "athena"
            },
            {
                "from_space": "nord",
                "rule": "assign",
                "source": "d200cbf9-b8c5-4639-8f90-d5e268731555:Second Click Value 'RESTART'",
                "space": "nord",
                "target": "d086c859-2e2f-4cc7-a9d0-f478706d50b0:Text: Hard Nordic Ship",
                "to_space": "athena"
            },
            {
                "delegate": {
                    "direction": "as_src",
                    "rule_type": "flow",
                    "uid": "on_click_0"
                },
                "from_space": "athena",
                "rule": "flow",
                "source": "65b77d0b-a4d0-4c30-8a5c-13bd61a84cba:Second Click Button",
                "space": "nord",
                "target": "d200cbf9-b8c5-4639-8f90-d5e268731555:Second Click Value 'RESTART'",
                "to_space": "nord"
            }
        ],
        "nodes": [
            {
                "id": "65b77d0b-a4d0-4c30-8a5c-13bd61a84cba",
                "name": "Second Click Button",
                "position": [
                    17.172075271606445,
                    -3.3749771118164062,
                    -0.3175033926963806
                ],
                "space": "athena",
                "type": "listener"
            },
            {
                "id": "981fb275-5096-4ca8-a10c-829ad293cd88",
                "name": "First Click Button",
                "position": [
                    -11.245755195617676,
                    1.6934242248535156,
                    -1.2562720775604248
                ],
                "space": "athena",
                "type": "listener"
            },
            {
                "id": "d086c859-2e2f-4cc7-a9d0-f478706d50b0",
                "name": "Text: Hard Nordic Ship",
                "position": [
                    3.6792008876800537,
                    0.5627975463867188,
                    -0.503038763999939
                ],
                "space": "athena",
                "type": "text"
            },
            {
                "id": "d200cbf9-b8c5-4639-8f90-d5e268731555",
                "name": "Second Click Value 'RESTART'",
                "position": [
                    11.668728828430176,
                    -0.021312713623046875,
                    -0.584818422794342
                ],
                "type": "static",
                "value": "RESTART"
            },
            {
                "id": "f8bafdf4-6f20-4e94-a622-c24596931e34",
                "name": "First Click Value 'CLICK'",
                "position": [
                    -4.737183570861816,
                    0.12541961669921875,
                    -1.235443353652954
                ],
                "type": "static",
                "value": "CLICK"
            },
            {
                "id": "fa866847-3d2f-40cd-8475-f6b1bd09c98c",
                "name": "Initial Value 'START'",
                "position": [
                    -4.292629718780518,
                    -2.235149383544922,
                    -6.6597113609313965
                ],
                "type": "static",
                "value": "START"
            }
        ]
    },
    "name": "Soggy Sanded Cave"
}