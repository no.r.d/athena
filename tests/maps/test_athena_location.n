{
    "extensions": {
        "athena": {
            "locations": {
                "code": [
                    "null://.",
                    "dir:///Users/nate/Desktop/Dev/NoRD/athena/src/nord/athena/",
                    "dir:///Users/nate/Desktop/Dev/NoRD/athena/src/nord"
                ],
                "files": [
                    "dir:///Users/nate/Desktop/Dev/NoRD/athena/src/nord/athena",
                    "dir:///Users/nate/Desktop/Dev/NoRD/athena/src/nord/athena/",
                    "dir:///Users/nate/Desktop/Dev/NoRD/athena/src/nord"
                ],
                "path": [
                    "dir:///Users/nate/Desktop/Dev/NoRD/athena/src/nord/athena",
                    "dir:///Users/nate/Desktop/Dev/NoRD/athena/src/nord/athena/",
                    "dir:///Users/nate/Desktop/Dev/NoRD/athena/src/nord"
                ]
            }
        },
        "fregata": {
            "locations": {
                "code": [
                    "null://."
                ],
                "files": [
                    "dir:///Users/nate/Desktop/Dev/NoRD/sandbox",
                    "dir:///Users/nate/Desktop/Dev/NoRD/fregata/src/nord/fregata"
                ],
                "path": [
                    "dir:///Users/nate/Desktop/Dev/NoRD/sandbox",
                    "dir:///Users/nate/Desktop/Dev/NoRD/fregata/src/nord/fregata"
                ]
            }
        },
        "pasion": {
            "locations": {
                "code": [
                    "null://."
                ],
                "files": [
                    "dir:///Users/nate/Desktop/Dev/NoRD/pasion/src/nord/pasion"
                ],
                "path": [
                    "dir:///Users/nate/Desktop/Dev/NoRD/pasion/src/nord/pasion"
                ]
            }
        },
        "wrigan": {
            "locations": {
                "code": [
                    "null://."
                ],
                "files": [
                    "dir:///Users/nate/Desktop/Dev/NoRD/wrigan/src/nord/wrigan"
                ],
                "path": [
                    "dir:///Users/nate/Desktop/Dev/NoRD/wrigan/src/nord/wrigan"
                ]
            }
        }
    },
    "map": {
        "edges": [
            {
                "from_space": "athena",
                "rule": "move",
                "source": "b0e26362-5f37-44da-9c91-45146e66cb32:Initial Location",
                "space": "athena",
                "target": "ca192da8-ec62-434b-813f-435779045de0:Second Location",
                "to_space": "athena"
            },
            {
                "from_space": "athena",
                "rule": "move",
                "source": "ab9b16bd-4e11-4baf-ad32-197458c3f972:Moved Listener",
                "space": "athena",
                "target": "b0e26362-5f37-44da-9c91-45146e66cb32:Initial Location",
                "to_space": "athena"
            },
            {
                "from_space": "athena",
                "rule": "attach",
                "source": "1ff4c866-764a-4ab1-8359-32379456206d:Attached Listener",
                "space": "athena",
                "target": "b0e26362-5f37-44da-9c91-45146e66cb32:Initial Location",
                "to_space": "athena"
            }
        ],
        "nodes": [
            {
                "id": "1ff4c866-764a-4ab1-8359-32379456206d",
                "name": "Attached Listener",
                "position": [
                    -11.26395320892334,
                    -2.3102951049804688,
                    8.950370788574219
                ],
                "space": "athena",
                "type": "listener"
            },
            {
                "id": "ab9b16bd-4e11-4baf-ad32-197458c3f972",
                "name": "Moved Listener",
                "position": [
                    -11.83464527130127,
                    0.2954216003417969,
                    -7.483145236968994
                ],
                "space": "athena",
                "type": "listener"
            },
            {
                "id": "b0e26362-5f37-44da-9c91-45146e66cb32",
                "name": "Initial Location",
                "position": [
                    -11.773213386535645,
                    7.62939453125e-06,
                    1.5915324687957764
                ],
                "space": "athena",
                "type": "location"
            },
            {
                "id": "ca192da8-ec62-434b-813f-435779045de0",
                "name": "Second Location",
                "position": [
                    6.767707824707031,
                    20.37739372253418,
                    3.066680908203125
                ],
                "space": "athena",
                "type": "location"
            }
        ]
    },
    "name": "Windy Viscous Sparrow"
}