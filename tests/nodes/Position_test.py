"""
Copyright 2020 NoRD Software Foundation

This file is part of NoRD.

NoRD is free software: you can redistribute it and/or modify
it under the terms of the GNU Affero General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

NoRD is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Affero General Public License for more details.

You should have received a copy of the GNU Affero General Public License
along with NoRD.  If not, see <https://www.gnu.org/licenses/>.
"""

import nord.athena.nodes._position as Position
import nord.athena.AthenaException as AthenaException
import nord.nord.Node as Node
import nord.nord.Map as Map

def test_create_position_from_arr():
    arr = [19.0, 22.3, 7.8]
    pos = Position(arr)
    assert pos.x == 19.0
    assert pos.y == 22.3
    assert pos.z == 7.8

    try:
        arr.pop()
        pos = Position(arr)
    except AthenaException as e:
        assert e.args[0] == 'Cannot initialize Position with [19.0, 22.3]'


def test_create_position_from_vals():
    x = 19.0
    y = 22.3
    z = 7.8
    pos = Position(None, x=x, y=y, z=z)
    assert pos.x == 19.0
    assert pos.y == 22.3
    assert pos.z == 7.8


def test_position_addition():
    arr = [19.0, 22.3, 7.8]
    p1 = Position(arr)
    arr = [19.0, 22.3, 7.8]
    p2 = Position(arr)
    p3 = p1 + p2
    assert p3.x == 38.0
    assert p3.y == 44.6
    assert p3.z == 15.6

    try:
        p3 = p1 + object()
    except AthenaException as e:
        assert e.args[0] == 'Position cannot add object'

def test_position_subtraction():
    arr = [19.0, 22.3, 7.8]
    p1 = Position(arr)
    arr = [19.0, 22.3, 7.8]
    p2 = Position(arr)
    p3 = p1 - p2
    assert p3.x == 0.0
    assert p3.y == 0.0
    assert p3.z == 0.0

    try:
        p3 = p1 - None
    except AthenaException as e:
        assert e.args[0] == 'Position cannot subtract NoneType'


def test_position_isequal():
    arr = [19.0, 22.3, 7.8]
    p1 = Position(arr)
    arr = [19.0, 22.3, 7.8]
    p2 = Position(arr)
    p3 = p1 == p2
    assert p3

    try:
        p3 = p1 == None
    except AthenaException as e:
        assert e.args[0] == 'Position cannot compare to NoneType'


def test_position_from_json():
    string = '{"x": 19.0, "y": 22.3, "z": 7.8}'
    pos = Position(None, in_json=string)
    assert pos.x == 19.0
    assert pos.y == 22.3
    assert pos.z == 7.8

    try:
        string = 'I AM NOT JSON'
        pos = Position(None, in_json=string)
    except AthenaException as e:
        assert e.args[0] == 'Cannot initialize Position from I AM NOT JSON'

    try:
        string = '{"x": 1, "y": 3, "string": "I AM WRONG JSON"}'
        pos = Position(None, in_json=string)
    except AthenaException as e:
        assert e.args[0] == 'Cannot initialize Position from {"x": 1, "y": 3, "string": "I AM WRONG JSON"}'


def test_set_node_position():
    node = Node(Map(None))
    string = '{"x": 19.0, "y": 22.3, "z": 7.8}'
    pos = Position(None, in_json=string)
    assert not hasattr(node, 'position')
    pos.apply_to_node(node)
    assert node.position == [19.0, 22.3, 7.8]
