"""
Copyright 2019 NoRD Software Foundation

This file is part of NoRD.

NoRD is free software: you can redistribute it and/or modify
it under the terms of the GNU Affero General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

NoRD is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Affero General Public License for more details.

You should have received a copy of the GNU Affero General Public License
along with NoRD.  If not, see <https://www.gnu.org/licenses/>.
"""
from nord.athena.nodes import Listener
from nord.nord.Runtime import Runtime
import nord.nord.Map as Map
import json
import pytest


def test_backend_execute():
    t = Listener(Map(None))
    t.execute(Runtime())
    assert t is not None


# @pytest.mark.skip(reason="THIS TEST ENDS UP BLOCKING WHEN RUN IN BATCH")
def test_on_click():
    """
    This test validates that an on_change
    event triggers the expected downmap
    subgraph execution.
    """
    try:
        m = json.load(open("maps/test_athena_listener.n", "r"))
    except FileNotFoundError:
        m = json.load(open("athena/tests/maps/test_athena_listener.n", "r"))
    karte = Map(m)
    vessal = Runtime()

    vessal.initialize(karte, verbose=True)
    vessal.begin()
    vessal.continue_exec()

    # Fires static with value: CLICK
    button1click = {
        "node_id": "981fb275-5096-4ca8-a10c-829ad293cd88",
        "type": "on_click",
        "payload": None
    }

    # Triggers static with value: RESTART
    button2click = {
        "node_id": "65b77d0b-a4d0-4c30-8a5c-13bd61a84cba",
        "type": "on_click",
        "payload": None
    }
    vessal.add_in_event(button1click)

    vessal.unpause()
    vessal.continue_exec()
    value = karte.nodes['d086c859-2e2f-4cc7-a9d0-f478706d50b0'].value
    assert value == 'CLICK'
    vessal.add_in_event(button2click)
    vessal.unpause()
    vessal.continue_exec()
    value = karte.nodes['d086c859-2e2f-4cc7-a9d0-f478706d50b0'].value
    assert value == 'RESTART'
    assert vessal.anomaly is None


@pytest.mark.skip(reason="THIS TEST ENDS UP BLOCKING WHEN RUN IN BATCH")
def test_text_output(): 
    """
    This test validates that an on_change
    event triggers the expected downmap
    subgraph execution.
    """
    try:
        m = json.load(open("maps/test_athena_listener.n", "r"))
    except FileNotFoundError:
        m = json.load(open("athena/tests/maps/test_athena_listener.n", "r"))
    karte = Map(m)
    vessal = Runtime()

    vessal.initialize(karte, verbose=True)
    vessal.begin()
    op = vessal.get_output('d086c859-2e2f-4cc7-a9d0-f478706d50b0')
    assert op == "START"
    button1click = {
        "node_id": "981fb275-5096-4ca8-a10c-829ad293cd88",
        "type": "on_click",
        "payload": None
    }

    # Triggers static with value: RESTART
    button2click = {
        "node_id": "65b77d0b-a4d0-4c30-8a5c-13bd61a84cba",
        "type": "on_click",
        "payload": None
    }
    vessal.add_in_event(button1click)

    vessal.unpause()
    vessal.continue_exec()
    op = vessal.get_output('d086c859-2e2f-4cc7-a9d0-f478706d50b0')
    assert op == "CLICK"

    vessal.add_in_event(button2click)

    vessal.unpause()
    vessal.continue_exec()
    op = vessal.get_output('d086c859-2e2f-4cc7-a9d0-f478706d50b0')
    assert op == "RESTART"


@pytest.mark.skip(reason="THIS TEST ENDS UP BLOCKING WHEN RUN IN BATCH")
def test_text_input_then_output(): 
    """
    This test validates that an on_change
    event triggers the expected downmap
    subgraph execution.
    """
    try:
        m = json.load(open("maps/test_athena_listener.n", "r"))
    except FileNotFoundError:
        m = json.load(open("athena/tests/maps/test_athena_listener.n", "r"))
    karte = Map(m)
    vessal = Runtime()

    vessal.initialize(karte, verbose=True)
    vessal.begin()
    op = vessal.get_output('d086c859-2e2f-4cc7-a9d0-f478706d50b0')
    assert op == "START"
    text1change = {
        "node_id": "d086c859-2e2f-4cc7-a9d0-f478706d50b0",
        "type": "on_change",
        "payload": "HELLO"
    }

    text1value = {
        "node_id": "d086c859-2e2f-4cc7-a9d0-f478706d50b0",
        "type": "on_value",
        "payload": "HELLO"
    }

    # Triggers static with value: RESTART
    button2click = {
        "node_id": "65b77d0b-a4d0-4c30-8a5c-13bd61a84cba",
        "type": "on_click",
        "payload": None
    }
    vessal.add_in_event(text1change)

    vessal.unpause()
    vessal.continue_exec()

    vessal.add_in_event(text1value)
    vessal.unpause()
    vessal.continue_exec()

    op = vessal.get_output('d086c859-2e2f-4cc7-a9d0-f478706d50b0')
    assert op == "HELLO"

    vessal.add_in_event(button2click)

    vessal.unpause()
    vessal.continue_exec()
    op = vessal.get_output('d086c859-2e2f-4cc7-a9d0-f478706d50b0')
    assert op == "RESTART"
