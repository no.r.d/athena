"""
Copyright 2020 NoRD Software Foundation

This file is part of NoRD.

NoRD is free software: you can redistribute it and/or modify
it under the terms of the GNU Affero General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

NoRD is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Affero General Public License for more details.

You should have received a copy of the GNU Affero General Public License
along with NoRD.  If not, see <https://www.gnu.org/licenses/>.
"""

import json
from nord.nord.Runtime import Runtime
import nord.nord.Map as Map
import nord.athena.nodes._position as Position
import pytest

@pytest.mark.skip(reason="THIS TEST ENDS UP BLOCKING WHEN RUN IN BATCH")
def test_move_and_attach():
    try:
        m = json.load(open("maps/test_athena_location.n", "r"))
    except FileNotFoundError:
        m = json.load(open("athena/tests/maps/test_athena_location.n", "r"))
    karte = Map(m)
    vessal = Runtime()

    attached_lstnr = karte.nodes['1ff4c866-764a-4ab1-8359-32379456206d']
    moved_lstnr = karte.nodes['ab9b16bd-4e11-4baf-ad32-197458c3f972']
    moved_location = karte.nodes['b0e26362-5f37-44da-9c91-45146e66cb32']
    end_location = karte.nodes['ca192da8-ec62-434b-813f-435779045de0']

    assert attached_lstnr.position == [-11.26395320892334, -2.3102951049804688, 8.950370788574219]
    assert moved_lstnr.position == [-11.83464527130127, 0.2954216003417969, -7.483145236968994]
    assert moved_location.position == [-11.773213386535645, 7.62939453125e-06, 1.5915324687957764]
    assert end_location.position == [6.767707824707031, 20.37739372253418, 3.066680908203125]

    start_moved_loc_pos = [] + moved_location.position

    vessal.initialize(karte, verbose=True)
    vessal.begin()
    vessal.continue_exec()

    assert attached_lstnr.position == [7.276968002319336, 18.06709098815918, 10.425519227981567]
    assert moved_lstnr.position == end_location.position
    assert moved_location.position == end_location.position

    pml = Position(moved_lstnr.position)
    pal = Position(attached_lstnr.position)
    pmo = Position(moved_location.position)
    assert vessal.out_events[2]["node_id"] == moved_lstnr.id
    assert vessal.out_events[2]["payload"] == pml.to_dict()
    assert vessal.out_events[1]["node_id"] == attached_lstnr.id
    assert vessal.out_events[1]["payload"] == pal.to_dict()
    assert vessal.out_events[0]["node_id"] == moved_location.id
    assert vessal.out_events[0]["payload"] == pmo.to_dict()


@pytest.mark.skip(reason="THIS TEST ENDS UP BLOCKING WHEN RUN IN BATCH")
def test_ping_pong():
    try:
        m = json.load(open("maps/test_athena_ping_pong.n", "r"))
    except FileNotFoundError:
        m = json.load(open("athena/tests/maps/test_athena_ping_pong.n", "r"))
    karte = Map(m)
    vessal = Runtime()

    # Left Button
    ping_click = {
        "node_id": "LEFT_BTN",
        "type": "on_click",
        "payload": None
    }

    # Rigt Button
    pong_click = {
        "node_id": "RIGHT_BTN",
        "type": "on_click",
        "payload": None
    }

    vessal.initialize(karte, verbose=True)
    vessal.begin()
    vessal.continue_exec()

    left_loc = karte.nodes["LEFT_LOC"]
    right_loc = karte.nodes["RIGHT_LOC"]
    ball_loc = karte.nodes["BALL"]
    attach_btn = karte.nodes["ATTACHED_BTN"]

    out_events = vessal.get_out_events()

    assert vessal.get_out_events() == []
    assert ball_loc.position == [0.40051549673080444, 3.680980682373047, -2.7966954708099365]
    assert attach_btn.position == [-0.06560724228620529, 0.6384162902832031, 4.147125720977783]
    assert out_events == []

    vessal.add_in_event(ping_click)

    vessal.unpause()
    vessal.continue_exec()
    out_events = vessal.get_out_events()

    assert vessal.get_out_events() == []
    assert ball_loc.position == left_loc.position
    assert attach_btn.position == [-16.369330517947674, -3.3869056701660156, 4.344561576843262]
    assert out_events == [
        {
            "node_id": "ATTACHED_BTN",
            "event_type": "move",
            "payload": {
                "x": -0.06560724228620529,
                "y": 0.6384162902832031,
                "z": 4.147125720977783
            }
        },
        {
            "node_id": "ATTACHED_BTN",
            "event_type": "move",
            "payload": {
                "x": -0.06560724228620529,
                "y": 0.6384162902832031,
                "z": 4.147125720977783
            }
        },
        {
            "node_id": "BALL",
            "event_type": "move",
            "payload": {
                "x": -15.903207778930664,
                "y": -0.3443412780761719,
                "z": -2.599259614944458
            }
        },
        {
            "node_id": "ATTACHED_BTN",
            "event_type": "move",
            "payload": {
                "x": -16.369330517947674,
                "y": -3.3869056701660156,
                "z": 4.344561576843262
            }
        }
    ]

    vessal.add_in_event(pong_click)

    vessal.unpause()
    vessal.continue_exec()
    out_events = vessal.get_out_events()

    assert vessal.get_out_events() == []
    assert ball_loc.position == right_loc.position
    assert attach_btn.position == [16.432772524654865, -3.0745468139648438, 4.54449462890625]
    assert out_events == [
        {
            "node_id": "BALL",
            "event_type": "move",
            "payload": {
                "x": 16.898895263671875,
                "y": -0.031982421875,
                "z": -2.3993265628814697
            }
        },
        {
            "node_id": "ATTACHED_BTN",
            "event_type": "move",
            "payload": {
                "x": 16.432772524654865,
                "y": -3.0745468139648438,
                "z": 4.54449462890625
            }
        }
    ]


    vessal.add_in_event(ping_click)

    vessal.unpause()
    vessal.continue_exec()
    out_events = vessal.get_out_events()

    assert vessal.get_out_events() == []
    assert ball_loc.position == left_loc.position
    assert attach_btn.position == [-16.369330517947674, -3.3869056701660156, 4.344561576843262]
    assert out_events == [
        {
            "node_id": "BALL",
            "event_type": "move",
            "payload": {
                "x": -15.903207778930664,
                "y": -0.3443412780761719,
                "z": -2.599259614944458
            }
        },
        {
            "node_id": "ATTACHED_BTN",
            "event_type": "move",
            "payload": {
                "x": -16.369330517947674,
                "y": -3.3869056701660156,
                "z": 4.344561576843262
            }
        }
    ]