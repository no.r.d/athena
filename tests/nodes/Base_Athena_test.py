"""
Copyright 2019 NoRD Software Foundation

This file is part of NoRD.

NoRD is free software: you can redistribute it and/or modify
it under the terms of the GNU Affero General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

NoRD is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Affero General Public License for more details.

You should have received a copy of the GNU Affero General Public License
along with NoRD.  If not, see <https://www.gnu.org/licenses/>.
"""

from nord.athena.nodes._base import flag_downstream_exe
import nord.nord.Map as Map  # noqa: E402
from nord.nord import Runtime as RT
Runtime = RT.Runtime


def test_flag_downstream_exe_one_way():
    graph = {
        "map": {
            "edges": [
                {
                    "rule":  "flow",
                    "source": "1:A",
                    "target": "2:B"
                },
                {
                    "rule":  "assign",
                    "source": "2:B",
                    "target": "3:C"
                },
                {
                    "rule":  "assign",
                    "source": "3:C",
                    "target": "4:D"
                },
                {
                    "rule":  "assign",
                    "source": "4:D",
                    "target": "5:E"
                }
            ],
            "nodes": [
                {
                    "id": "1",
                    "name": "A",
                    "type": "listener",
                    "space": "athena"
                },
                {
                    "id": "2",
                    "name": "B",
                    "type": "data"
                },
                {
                    "id": "3",
                    "name": "C",
                    "type": "data"
                },
                {
                    "id": "4",
                    "name": "D",
                    "type": "data"
                },
                {
                    "id": "5",
                    "name": "E",
                    "type": "data"
                }
            ]
        }
    }
    karte = Map(graph, verbose=True)
    vessal = Runtime()
    node = karte.nodes["1"]
    flag_downstream_exe(node, vessal)
    assert not karte.nodes["1"]._run_state._athena_downstream_flagged
    assert not karte.nodes["2"]._run_state._athena_downstream_flagged
    assert not karte.nodes["3"]._run_state._athena_downstream_flagged
    assert not karte.nodes["4"]._run_state._athena_downstream_flagged
    assert not karte.nodes["5"]._run_state._athena_downstream_flagged
    assert karte.nodes["1"]._run_state._executed == -1
    assert karte.nodes["2"]._run_state._executed == 0
    assert karte.nodes["3"]._run_state._executed == 0
    assert karte.nodes["4"]._run_state._executed == 0
    assert karte.nodes["5"]._run_state._executed == 0


def test_flag_downstream_exe_loop():
    graph = {
        "map": {
            "edges": [
                {
                    "rule":  "flow",
                    "source": "1:A",
                    "target": "2:B"
                },
                {
                    "rule":  "assign",
                    "source": "2:B",
                    "target": "3:C"
                },
                {
                    "rule":  "assign",
                    "source": "3:C",
                    "target": "4:D"
                },
                {
                    "rule":  "assign",
                    "source": "4:D",
                    "target": "5:E"
                },
                {
                    "rule":  "assign",
                    "source": "5:E",
                    "target": "2:B"
                }
            ],
            "nodes": [
                {
                    "id": "1",
                    "name": "A",
                    "type": "listener",
                    "space": "athena"
                },
                {
                    "id": "2",
                    "name": "B",
                    "type": "data"
                },
                {
                    "id": "3",
                    "name": "C",
                    "type": "data"
                },
                {
                    "id": "4",
                    "name": "D",
                    "type": "data"
                },
                {
                    "id": "5",
                    "name": "E",
                    "type": "data"
                }
            ]
        }
    }
    karte = Map(graph, verbose=True)
    vessal = Runtime()
    node = karte.nodes["1"]
    flag_downstream_exe(node, vessal)
    assert not karte.nodes["1"]._run_state._athena_downstream_flagged
    assert not karte.nodes["2"]._run_state._athena_downstream_flagged
    # assert not karte.nodes["3"]._run_state._athena_downstream_flagged
    # assert not karte.nodes["4"]._run_state._athena_downstream_flagged
    # assert not karte.nodes["5"]._run_state._athena_downstream_flagged
    # assert karte.nodes["1"]._run_state._executed == 0
    assert karte.nodes["2"]._run_state._executed == 0
    # assert karte.nodes["3"]._run_state._executed == 0
    # assert karte.nodes["4"]._run_state._executed == 0
    # assert karte.nodes["5"]._run_state._executed == 0
