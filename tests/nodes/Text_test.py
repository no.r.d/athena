"""
Copyright 2019 NoRD Software Foundation

This file is part of NoRD.

NoRD is free software: you can redistribute it and/or modify
it under the terms of the GNU Affero General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

NoRD is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Affero General Public License for more details.

You should have received a copy of the GNU Affero General Public License
along with NoRD.  If not, see <https://www.gnu.org/licenses/>.
"""
from nord.athena.nodes import Text
from nord.nord.Runtime import Runtime
import nord.nord.Map as Map
import pytest
import nord.sigurd.shell.vismap as vm
import nord.sigurd.managers.containermanager as cm
import nord.sigurd.managers.objectmanager as om
from pathlib import Path
from panda3d.core import Point3


def test_backend_execute():
    t = Text(Map(None))
    t.execute(Runtime())
    assert t is not None


@pytest.mark.skip(reason="INCOMPLETE TEST")
def test_on_change():
    """
    TODO: Implement this test
    This test validates that an on_change
    event triggers the expected downmap
    subgraph execution.
    """
    assert 1 == 2


@pytest.mark.skip(reason="INCOMPLETE TEST")
def test_on_value():
    """
    TODO: Implement this test
    This test validates that an on_value
    event triggers the expected downmap
    subgraph execution.
    """
    assert 2 == 3


def test_text_inside_container(init_panda3d):
    app = init_panda3d
    if Path('./athena/tests/maps/test_athena_contained_text.n').exists():
        app.load_map('./athena/tests/maps/test_athena_contained_text.n')
    elif Path('./tests/maps/test_athena_contained_text.n').exists():
        app.load_map('./tests/maps/test_athena_contained_text.n')
    else:
        raise Exception("Cannot find test map to load")
    karte = vm.get_working_map()
    cm.enter("2fc99f25-3826-4f43-83e6-5c4d936b3c2c")
    # Currently, the line below crashes in callbackmanager.py:224 as is_empty doesn't exist
    vm.create_node("Text", Point3(1.0, 0.0, 1.0), "athena", containername="2fc99f25-3826-4f43-83e6-5c4d936b3c2c")
    text = om.fetch("2fc99f25-3826-4f43-83e6-5c4d936b3c2c")
    assert not text.data.is_empty()
    # while True:
    #     app.one_loop_iteration()
