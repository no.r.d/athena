"""
Copyright 2021 NoRD Software Foundation

This file is part of NoRD.

NoRD is free software: you can redistribute it and/or modify
it under the terms of the GNU Affero General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

NoRD is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Affero General Public License for more details.

You should have received a copy of the GNU Affero General Public License
along with NoRD.  If not, see <https://www.gnu.org/licenses/>.

--
"""
from panda3d.core import loadPrcFileData
from panda3d.core import Point3
import nord.sigurd.managers.modelmanager as mm
import nord.sigurd.shell.vismap as vm
import pytest
import builtins


@pytest.fixture(scope="function")
def init_panda3d():
    if hasattr(builtins, "base"):
        base.destroy()  # noqa: F821
    loadPrcFileData("", "window-type offscreen")
    import nord.sigurd.shell.application as app
    app.prep_to_run()
    app.mapevents()
    vm.unload()
    # import sys
    # sys.stderr.write("\n\n\n%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%\n\n\n")
    # import nord.sigurd.shell.mainmenu as mainmenu
    # mainmenu.internal_reinit_module()
    try:
        mm.load("tests/config/modelmap.conf", space="nord")
    except FileNotFoundError:
        mm.load("sigurd/tests/config/modelmap.conf", space="nord")
    yield app
    render.clear()  # noqa: F821
    # render.get_children().detach()  # noqa: F821
    app._cleanup_instance()
    # app.shutdown()
    base.destroy()  # noqa: F821


@pytest.fixture(autouse=True)
def patch_getTightBounds(monkeypatch):
    from nord.sigurd.shell.menuItem import MenuItem

    def gtb(*args, **kwargs):
        return (Point3(0.0, 0.0, 0.0), Point3(1.0, 1.0, 1.0))
    monkeypatch.setattr(MenuItem, 'getTightBounds', gtb)
    from nord.sigurd.shell.model import Model
    monkeypatch.setattr(Model, 'getTightBounds', gtb)
    from nord.sigurd.visibles.comment import Comment
    monkeypatch.setattr(Comment, 'getTightBounds', gtb)
    from nord.sigurd.shell.worldview import WorldView
    monkeypatch.setattr(WorldView, 'set_fog', gtb)
    monkeypatch.setattr(WorldView, 'set_lighting', gtb)
