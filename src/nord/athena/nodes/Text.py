"""
Copyright 2019 NoRD Software Foundation

This file is part of NoRD.

NoRD is free software: you can redistribute it and/or modify
it under the terms of the GNU Affero General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

NoRD is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Affero General Public License for more details.

You should have received a copy of the GNU Affero General Public License
along with NoRD.  If not, see <https://www.gnu.org/licenses/>.
"""

from nord.athena.nodes._base import Base
import sys


class Text(Base):
    """"""
    def _on_change(self, runtime, event):
        """
        Deal with each keypress in the text area
        """
        if "payload" not in event:
            runtime.setAnomaly(Exception("Athena Text Error: missing payload"))
            return True

        self.set_value(event['payload'])

    def _on_value(self, runtime, event):
        """
        Deal with changes in value of the visible.

        Return True if the event is malformed.
        A la: not having a payload
        """
        if "payload" not in event:
            runtime.setAnomaly(Exception("Athena Text Error: missing payload"))
            return True

        self.set_value(event['payload'])

        """
        If on_value is connected (has a delegated connection),
        set this node as next to be executed such that
        the runtime will execute this node.

        Probably need to follow any and all source edges to
        in order to establish the appropriate state
        """
        execute_self = False
        # for edge in self.get_targets('pass'):
        #     if hasattr(edge, 'delegate'):
        #         execute_self = True
        #         break

        # if not execute_self:
        #     for edge in self.get_targets('assign'):
        #         if hasattr(edge, 'delegate'):
        #             execute_self = True
        #             break

        # if execute_self:
        #     runtime.setNextNode(self, 'DOWN')

        # return False





        """
        IMPORTANT: The return value of False
        Is what allows the runtime to
        to continue processing downstream
        edges and nodes. So, if any of the process_event_rule
        functions return false, then, this function needs to
        return false.
        """
        rv1 = self.process_event_rule('assign', runtime, event, block_undelegated=True)
        rv2 = self.process_event_rule('pass', runtime, event, block_undelegated=True)

        rv = True
        if not rv1 or not rv2:
            rv = False

        return rv

    def handle_publish(self, runtime, event):
        """
        The runtime has detected an event, that this
        node has subscribed to. Trigger the requested
        processing (starting with this node)

        >>>>>>>>>>>>NOT PRESENTLY CALLED ANYWHERE <<<<<<<<<<<
        """
        self.handle_event(runtime)
        if hasattr(self, '_ev_subscriptions') and\
                event in self._ev_subscriptions:
            for source in self._ev_subscriptions[event]:
                source.clear_exe(runtime)
                runtime.appendNextNode(source, 'DOWN')

    def set_value(self, value):
        self.value = value
        # rt = self.get_op_runtime()
        # if rt:
        #     rt.set_output(self.id, self.value)

    def get_value(self):
        return self.value

    def has_value(self):
        return hasattr(self, 'value')


sys.modules[__name__] = Text
