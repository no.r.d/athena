"""
Copyright 2019 NoRD Software Foundation

This file is part of NoRD.

NoRD is free software: you can redistribute it and/or modify
it under the terms of the GNU Affero General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

NoRD is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Affero General Public License for more details.

You should have received a copy of the GNU Affero General Public License
along with NoRD.  If not, see <https://www.gnu.org/licenses/>.
"""
import nord.nord.Node as Node
import nord.nord.Runtime as RT
RS = RT.RS


def set_downstream_event_id(node, eventor_id):
    RS.s(node).set_upstream_eventor(eventor_id)
    for rule in node.get_target_rules():
        # if rule in ['attach', 'move']:
        #     continue
        for edge in node.get_targets(rule):
            set_downstream_event_id(edge.get_target(), eventor_id)


def flag_downstream_exe(node, runtime):
    """
    Given the input node, mark all downstream
    node's executed, unless they are already so marked
    or, if there are more than one path into said node.
    """
    if RS.s(node).athena_downstream_flagged():
        return
    if not node.get_cfg_value('pre_event_flag_multi_source_downstream'):
        """
        If the configuration says it does
        not want to flag downstream nodes
        with multiple sources, then, employ the check's
        below to stop recursing.
        """
        if len(node.get_source_rules()) > 1:
            if node.get_cfg_value('pre_event_stage_multi_for_exec'):
                runtime.setNextNode(node, 'DOWN')
            RS.s(node).unset_athena_downstream_flagged()
            return
        for rule in node.get_source_rules():
            # if rule in ['attach', 'move']:
            #     continue
            if len(node.get_sources(rule)) > 1:
                if node.get_cfg_value('pre_event_stage_multi_for_exec'):
                    runtime.setNextNode(node, 'DOWN')
                RS.s(node).unset_athena_downstream_flagged()
                return
    RS.s(node).set_athena_downstream_flagged()
    if node.has_executed():
        RS.s(node).unset_athena_downstream_flagged()
        return
    if not len(node.get_target_rules()):
        RS.s(node).unset_athena_downstream_flagged()
        return
    for rule in node.get_target_rules():
        # if rule in ['attach', 'move']:
        #     continue
        for edge in node.get_targets(rule):
            flag_downstream_exe(edge.get_target(), runtime)
            edge.get_target().flag_exe(runtime)
            edge.flag_followed(runtime)
            RS.s(edge.get_target()).unset_athena_downstream_flagged()
    RS.s(node).unset_athena_downstream_flagged()


def clear_downstream_exe(node, runtime):
    """
    Given the input node, mark all downstream
    node's executed, unless they are already so marked
    or, if there are more than one path into said node.
    """
    if RS.s(node).athena_downstream_flagged():
        RS.s(node).unset_athena_downstream_flagged()
        return

    # if node.has_executed():
    #     RS.s(node).unset_athena_downstream_flagged()
    #     return
    if not len(node.get_target_rules()):
        RS.s(node).unset_athena_downstream_flagged()
        return
    RS.s(node).set_athena_downstream_flagged()
    for rule in node.get_target_rules():
        for edge in node.get_targets(rule):
            clear_downstream_exe(edge.get_target(), runtime)
            edge.get_target().clear_exe(runtime)
            edge.clear_followed()
            RS.s(edge.get_target()).unset_athena_downstream_flagged()
    RS.s(node).unset_athena_downstream_flagged()


class Base(Node):
    """
    Config Items:
      pre_event_flag_multi_source_downstream - controls whether
      or not to continue downstream flagging as executed (in this
      nodes initial execution) if the downstream node has more than
      one source (obviously, in addition to this node). The default is
      True.

      pre_event_stage_multi_for_exec - controls whether or not
      to request downstream nodes with multiple sources
      be executed subsequent to this node in the current
      Frame. Depends upon pre_event_flag_multi_source_downstream
      (PEFMSD) being set to False. If PEFMSD is True, then
      staging would be meaningless as the nodes will appear
      to have run already. NOTE: Setting this to true seems harmful.
      At the very least, it breaks Listener_test::test_on_click
    """

    _CI_pre_event_flag_multi_source_downstream = False
    _CI_pre_event_stage_multi_for_exec = False

    # For RuntimeStateMachine based runtime
    _CI_continue_exec_wo_event = False

    # As these nodes emit events
    # they need to have their flow edges
    # back-propogated up, so they can
    # perform their initial execution
    # which establishes the state needed to
    # process events later.
    _CI_event_emitting_node = True

    # This is needed to prevent the pre-run of the map from starting
    # with a node which might be expecting events, (like it's value to be set from an event)
    # which might have downstream edges which depend upon the event data
    # Specifically, in a simple map where Text nodes are sourceless, but, have targets
    # which are passing or assigning the value from teh text node, the value is not
    # yet set, prior to the event arrival, but, the pre-run of the map, would try to run the text, and downstream
    # nodes, which then get stuck in a permanently pre-empted state, and teh runtime loops forever, without checking for
    # any events.
    # It may be worth checking for events in the pre-empted state, but, there might be serious consequences of that...
    _CI_cannot_start_map = True

    # ??????????? DO I NEED THIS ????????????
    _CI_event_exec_clear_multi_source_downstream = True
    # !!!!!!!!!! IF SO, DON"T FORGET TO DOCUMENT !!!!!!!!!!!!!!!!!!!!

    def prep_to_execute(self, runtime, verbose):
        """
        Because move and attach are special edges, we need to
        ensure they are followed, regardless of the EHC status IF
        those edges are trying to move this node.

        So, that means a edge whose source is this node
        and it is an attach or move edge, we need to make sure
        it is followed before executing this node. That is because
        this node may be visually moved.
        """

        # Save the runtime so it can be referenced
        # in subsequent calls.
        self._current_runtime = runtime

        for rule in self.get_target_rules():
            # if the first target already has indicates this node
            # as an eventing parent, assume all have and stop.
            if RS.s(self.get_targets(rule)[0].get_target()).downstream_of_eventor(self.id):
                break
            for edge in self.get_targets(rule):
                set_downstream_event_id(edge.get_target(), self.id)

        if not hasattr(self, '_unfollowedSources'):
            return False

        # unfollowed = self._unfollowedSources.copy()
        unfollowed = self.get_unfollowed_sources(runtime)
        for edge in unfollowed:
            if edge.rule in ['move', 'attach']:
                edge.follow(runtime, verbose)

        return True
        # new runtime state machine obviates the need to
        # call the superclass prep_to_execute.
        # Node.prep_to_execute(self, runtime, verbose)

    def execute(self, runtime):
        """
        When an Athena UI node is executed,
        it tricks the runtime into thinking
        the downstream nodes have executed
        by flagging the immediately connected
        nodes an executed.
        """
        # if hasattr(self, '_event_ready'):
        if runtime.in_this_node_event(self):
            """
            This node has been run, at least
            once. Any subsequent execution
            is therefore in response to an event.
            So, all outbound edges should be able to
            be executed.
            """
            clear_downstream_exe(self, runtime)
            super().execute(runtime)
            # if hasattr(self, 'id') and runtime._in_event == self.id:
            #     runtime.pause()
            return

        # But, if it hasn't received any events, mark all outbound
        # edges as followed, to prevent reachback triggered execution.
        # And, traverse all the way "down" the graph, such that all downstream
        # nodes and edges are marked. Process downwards until there are no
        # target_rules, and/or a loop is detected.
        flag_downstream_exe(self, runtime)

        if not hasattr(self, '_event_ready'):
            self._event_ready = True

        self.flag_exe(runtime)
        # If the runtime is processing an event, that I did not originate
        # then, I should not pause the runtime....
        # if hasattr(self, 'id') and runtime._in_event == self.id:
        #     runtime.pause()

    def handle_event(self, runtime, event):
        """
        Return True to stop the runtime,
        False to have the runtime continue

        Set the next node to move execution
        to that node. The runtime should have
        nothing but Events to process at this
        point.

        The event structure is, at a minimum:
        {
            "node_id": "<SOME GUID>"
        }

        Athena events will look like:
        {
            "node_id": "<SOME GUID>",
            "type": "<an event name>",
            "payload": "Optional, a json serializable value"
        }

        The value in type must correspond to a method
        in the requested node. For instance:

        Node with ID 123 looks like:

        class Button(Base):
            _on_click(self, runtime, event):
                pass

        It can then respond to events that look like:
        {
            "node_id": 123,
            "type": "on_click",
            "payload": "some stuff"
        }
        """
        if "type" not in event:
            runtime.setAnomaly(Exception(f"Athena Error: Malformed event, 'type' missing: {event}"))
            return True
        if not hasattr(self, f"_{event['type']}"):
            runtime.setAnomaly(Exception(f"Athena Error: Misdirected event, cannot process {event}"))
            return True
        func = getattr(self, f"_{event['type']}")
        # The following line is not the correct way to solve the problem
        # runtime.reset_all_nodes_runstate()
        # Upon the first run through the map, some of the nodes are left in a pending run state
        # such that the runtime will not add them to the frames next_nodes queue
        # and thereby will not execute them. walujapi#32 issue was opened WRT this.
        return func(runtime, event)

    def process_event_rule(self, rulename, runtime, event, block_undelegated=False):
        """
        Stage for execution all the targets of
        outbound edges matching the given rulename.

        Flag this node as executed. Return True if
        no targets were added.
        """
        runtime.replace_current_node(self, 'DOWN')  # should be fine since the current node should be None at this point
        # runtime.pushStack()
        # self.flag_preempted()
        rv = True
        # for edge in self.get_targets(rulename):
        #     if "payload" in event:
        #         edge.set_cargo(event["payload"])
        #     edge.follow(runtime, runtime.verbose)
        #     runtime.setNextNode(edge.get_target(), 'DOWN')
        #     rv = False

        # return rv

        for edge in self.get_targets(rulename):
            """
            If the event provided has payload, place it on the edge
            as cargo, The source rule can override the cargo value
            and likely will in pass and assign cases.
            """
            if "payload" in event:
                edge.set_cargo(event["payload"])

            """
            If this edge is delegated (we should probably consider source or
            target delegation here, but presently are not)
            then, schedule this node to be run next
            """
            if hasattr(edge, 'delegate'):
                runtime.setNextNode(self, 'DOWN')
                rv = False
                break
                """
                But, a node that can handle delegation, can specify whether or
                not it wants to trigger its
                downstream nodes in the event it is not delegated.
                """
            elif not block_undelegated:
                edge.follow(runtime, runtime.verbose)
                runtime.setNextNode(edge.get_target(), 'DOWN')
                runtime.unpause()
                rv = False
        return rv
