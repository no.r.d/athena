"""
Copyright 2020 NoRD Software Foundation

This file is part of NoRD.

NoRD is free software: you can redistribute it and/or modify
it under the terms of the GNU Affero General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

NoRD is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Affero General Public License for more details.

You should have received a copy of the GNU Affero General Public License
along with NoRD.  If not, see <https://www.gnu.org/licenses/>.
--
NOTE: There should be a convergence between this Position
class and nord.nord.Base WRT the position comparisons.

There should not be scattered implementations of the position logic.
"""
import nord.athena.AthenaException as AthenaException
import nord.nord.Strings  # noqa: F401 # imported for _ function
import json
import sys


class Position(object):
    """"""
    def __init__(self, arr, x=None, y=None, z=None, in_json=None):
        if x is not None and y is not None and z is not None:
            self.x = x
            self.y = y
            self.z = z
            return

        if in_json is not None:
            try:
                d = json.loads(in_json)
                self.x = d['x']
                self.y = d['y']
                self.z = d['z']
                return
            except Exception:
                raise AthenaException(_("Cannot initialize Position from ") + f"{in_json}")  # noqa: F821

        if arr is None or len(arr) != 3:
            raise AthenaException(_("Cannot initialize Position with ") + f"{arr}")  # noqa: F821

        self.x = arr[0]
        self.y = arr[1]
        self.z = arr[2]

    def __sub__(self, other):
        if not isinstance(other, Position):
            raise AthenaException(_("Position cannot subtract ") + f"{other.__class__.__name__}")  # noqa: F821
        dx = self.x - other.x
        dy = self.y - other.y
        dz = self.z - other.z

        return Position(None, x=dx, y=dy, z=dz)

    def __add__(self, other):
        if not isinstance(other, Position):
            raise AthenaException(_("Position cannot add ") + f"{other.__class__.__name__}")  # noqa: F821
        dx = self.x + other.x
        dy = self.y + other.y
        dz = self.z + other.z

        return Position(None, x=dx, y=dy, z=dz)

    def __eq__(self, other):
        if not isinstance(other, Position):
            raise AthenaException(_("Position cannot compare to ") + f"{other.__class__.__name__}")  # noqa: F821
        return self.x == other.x and self.y == other.y and self.z == other.z

    def to_dict(self):
        return {
            "x": self.x,
            "y": self.y,
            "z": self.z
        }

    def to_json(self):
        return json.dumps(self.to_dict())

    def apply_to_node(self, node):
        """
        Set the node's position attribute
        according to this instance's members.
        """
        node.position = [
            self.x,
            self.y,
            self.z
        ]

    def apply_to_visnode(self, visnode):
        """
        Move the visible node to this position.
        """
        visnode.setPos(self.x, self.y, self.z)


sys.modules[__name__] = Position
