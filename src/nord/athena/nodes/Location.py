"""
Copyright 2019 NoRD Software Foundation

This file is part of NoRD.

NoRD is free software: you can redistribute it and/or modify
it under the terms of the GNU Affero General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

NoRD is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Affero General Public License for more details.

You should have received a copy of the GNU Affero General Public License
along with NoRD.  If not, see <https://www.gnu.org/licenses/>.
"""

import nord.nord.Node as Node
import nord.athena.nodes._position as Position
import nord.athena.AthenaException as AthenaException
import sys


def get_position(node):
    """
    Create a position instance from the given node.
    """
    if hasattr(node, 'position'):
        return Position(node.position)
    else:
        raise AthenaException(_("IMPLEMENTATION ERROR: Position member not present."))  # noqa: F821


def set_position(node, position):
    """
    Given an input position, set the node's
    position attribute accordingly.
    """
    node._prior_position = get_position(node)
    position.apply_to_node(node)
    runtime = node.get_op_runtime()
    if runtime:
        runtime.add_out_event({"node_id": node.id, "event_type": "move", "payload": position.to_dict()})


class Location(Node):
    """"""
    def execute(self, runtime):
        """
        Prep to execute, should have handled any move edges
        into this node which will have set its location. Such that
        when this node executes, it affects the locations of all
        attached nodes.

        Send an event to all attached nodes of current location.

        Then, next_after_execute will, if so configured, flag downstream
        (down move edges) as executed in order to wait for another
        triggering event (wait is default config?)

        If there is an unfollowed move target (meaning, this node wants to move,
        Run that node before this one (so, pre-empt))
        """

        if not hasattr(self, '_prior_position'):
            self._prior_position = get_position(self)

        self.flag_exe(runtime)
        self._position_delta = self._prior_position - get_position(self)

    def get_position_delta(self):
        if not hasattr(self, '_position_delta'):
            return Position(None, x=0.0, y=0.0, z=0.0)
        else:
            return self._position_delta

    @staticmethod
    def get_position(node):
        """
        StaticMethod wrapper to expose the global function
        since we are replacing the module with the class definition.
        """
        return get_position(node)

    @staticmethod
    def set_position(node, position):
        """
        StaticMethod wrapper to expose the global function
        since we are replacing the module with the class definition.
        """
        set_position(node, position)


sys.modules[__name__] = Location
