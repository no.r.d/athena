"""
Copyright 2019 NoRD Software Foundation

This file is part of NoRD.

NoRD is free software: you can redistribute it and/or modify
it under the terms of the GNU Affero General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

NoRD is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Affero General Public License for more details.

You should have received a copy of the GNU Affero General Public License
along with NoRD.  If not, see <https://www.gnu.org/licenses/>.
"""

from nord.athena.nodes._base import Base
import sys


class Listener(Base):
    """"""

    def _on_click(self, runtime, event):
        """
        Upon being clicked, a listener
        needs to trigger the execution of any connected
        downstream nodes. (If there are any connected...)
        The UI should not send this event if the Map does not
        have any downstream nodes.
        """

        if "payload" in event and event["payload"] is not None:
            runtime.setAnomaly(Exception("Athena Listener Error: Unexpected payload"))
            return True

        rv = self.process_event_rule('flow', runtime, event)
        print(f"\n\n------------------------ click: {self} ------\n\n")
        return rv


sys.modules[__name__] = Listener
