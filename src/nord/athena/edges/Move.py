"""
Copyright 2019 NoRD Software Foundation

This file is part of NoRD.

NoRD is free software: you can redistribute it and/or modify
it under the terms of the GNU Affero General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

NoRD is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Affero General Public License for more details.

You should have received a copy of the GNU Affero General Public License
along with NoRD.  If not, see <https://www.gnu.org/licenses/>.
"""

import nord.nord.Edge as Edge
import sys


class Move(Edge):
    """"""
    _operate_in_reverse = True

    def traverse(s, srule, trule):
        """
        The Move edge operates somewhat backwards from
        typical edges. It collects the position from the Target
        and makes it available to the Source. This is done this
        way, because the UX is more intuitive to Move the Source
        to the Target position, rather than the other way around.

        NOTE: Moved Node's will have backend position's updated
        (that is to say the graph / mapentry will have an updated
        position within the backend runtime (walujapi)), but, the
        sigurd (frontend) map will not have updated position data,
        but, the visible position will be changed.
        
        NOTE: Should use operate in reverse (like attach....) maybe???
        """
        if trule.apply():
            return srule.apply()
        return False


sys.modules[__name__] = Move
