"""
Copyright 2019 NoRD Software Foundation

This file is part of NoRD.

NoRD is free software: you can redistribute it and/or modify
it under the terms of the GNU Affero General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

NoRD is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Affero General Public License for more details.

You should have received a copy of the GNU Affero General Public License
along with NoRD.  If not, see <https://www.gnu.org/licenses/>.
"""

from nord.athena.visibles._base import Base as Base
from nord.sigurd.shell.visnode import VisNode
from nord.sigurd.utils.log import note
from nord.sigurd.utils.verboser import funlog


class Listener (Base, VisNode):
    def __init__(self, uid, app):
        """
        Register the events to handle with the parent class.
        """
        VisNode.__init__(self, uid, 'listener', app, space="athena")
        self._delegated_events = {
            'on_click': {'model': 'action', 'rule': 'flow', "s_or_t": "as_src", 'callback': self.conn_event},
        }
        Base.__init__(self)

    @funlog
    def on_click(self):
        """
        Evaluate the this node's map entry to
        see if it is the source of any flow edges.

        Only send the click event if there are flow
        edges leaving this node.
        """
        if self.mapentry.get_targets('flow'):
            self.send_event("on_click", None)
        else:
            note(f"Click detected on {self}, but not source of any flow edges.")
