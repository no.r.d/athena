"""
Copyright 2019 NoRD Software Foundation

This file is part of NoRD.

NoRD is free software: you can redistribute it and/or modify
it under the terms of the GNU Affero General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

NoRD is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Affero General Public License for more details.

You should have received a copy of the GNU Affero General Public License
along with NoRD.  If not, see <https://www.gnu.org/licenses/>.
"""
from nord.sigurd.utils.verboser import funlog  # noqa: F401
from nord.wrigan.net.utils import put
from nord.wrigan.visibles.utils import show_error
import nord.sigurd.managers.statemanager as sm
import nord.sigurd.managers.statemanager as em
from nord.wrigan.visibles.runmenu import get_running_map_id, gather_show_outputs
import nord.athena.nodes._position as Position
import nord.sigurd.managers.event as event
from nord.sigurd.utils.log import warn


class Base(object):
    def __init__(self):
        self.add_filter_category('execvisible')
        self._priorAlpha = None
        self._mapped_events = None

        # need to create a submenu
        # under show_delegates for all
        # _delegated_events entries
        self.onMappedFuncs.append(self.map_events_to_rules)
        # self.onMappedFuncs.append(establish_event_fetcher)

    # @funlog
    def is_visible(self):
        """Return whether or not this model has been hidden."""
        return self.visible

    @event.callback
    # @funlog
    def show_menu(self, *args, **kwargs):
        """
        NOTE: self.menu is initialized in Text.py
        """
        self.menu.show(self.getPos(), None)

    @event.callback
    # @funlog
    def show_debug_menu(self, *args, **kwargs):
        """
        NOTE: self.debug_menu is initialized in Text.py
        """
        self.debug_menu.show(self.getPos(), None)

    # @funlog
    def map_events_to_rules(self):
        """
        As this cannot be called until the
        map has been fully loaded, we'll call
        this method the first time any event is
        to be sent.

        This is a crappy. add_delegate comes from
        the CallbackManager class, which Base is NOT
        a descendent of.
        """
        if not hasattr(self, '_delegated_events'):
            warn("NO MAPPED EVENTS")
            return
        if self._mapped_events is None:
            self._mapped_events = {}
            if hasattr(self, '_delegated_events'):
                for evtnm in self._delegated_events:
                    mapd = self._delegated_events[evtnm]
                    if type(mapd) is not list:
                        mapd = [mapd]
                    for idx, md in enumerate(mapd):
                        dec = self.add_delegate(f"{evtnm}_{idx}",
                                                md['model'],
                                                md['rule'],
                                                md['s_or_t'],
                                                extra=(evtnm, md['callback']))

                        # Set the tooltip to the event name and the rule
                        # For instance "on_click pass"

                        def gen_updater(x):
                            def athena_tt_updater(decoration):
                                decoration.set_text(x)
                            return athena_tt_updater

                        dec.set_tooltip_updater(gen_updater(f"{evtnm} {md['rule']}"))

    @funlog
    def process_delegated_click(self, mapentry, payload):
        """
        given the _delegated_events, parse the payload
        and find the appropriate callback in the
        descendent class, then call it.

        payload looks like:
        {"direction": direction, "rule": rule_type, "decoration": dec, "extra": (event_name, callback)}

        """
        dec = payload["decoration"]
        rule = payload["rule"]
        direction = payload["direction"]
        ev_type = payload["extra"][0]
        cbk = payload["extra"][1]
        cbk(dec, rule, direction, ev_type)

    @funlog
    def send_event(self, evtype, payload):
        event = {
            "node_id": self.mapentry.id,
            "type": evtype,
            "payload": payload
        }
        mapid = get_running_map_id()
        if mapid is None:
            show_error("Map not running, not sure how we got here....")
        resp = put(f"event/{mapid}", data=event)
        if resp.status_code != 200:
            show_error(resp.text)
            em.onmapstop()

        gather_show_outputs()

    @funlog
    def handle_event(self, event):
        """
        Base method used to deal with runtime out events.

        Interrogate's itself, for a method named "on_{event_type}".
        If not found render an anomaly, if found,
        calls the method, passing it the payload.
        """
        if "event_type" not in event:
            show_error(f"Malformed event, missing event_type: {event}")
            return
        if "payload" not in event:
            show_error(f"Malformed event, missing payload: {event}")
            return
        if not hasattr(self, f"on_{event['event_type']}"):
            show_error(f"IMPLEMENTATION ERROR: {self} missing method on_{event['event_type']}")
            return
        func = getattr(self, f"on_{event['event_type']}")
        func(event["payload"])

    @funlog
    def on_click(self):
        """
        Override-able on-click handler.

        The default action is to send
        the click action to the backend.
        """
        self.send_event("on_click", None)

    @funlog
    def on_move(self, payload):
        pos = Position(None, x=payload['x'], y=payload['y'], z=payload['z'])
        pos.apply_to_visnode(self)

    @funlog
    def conn_event(self, dec, rule, direction, ev_type):
        """
        Callback to be called  when
        one of my delegates is clicked upon to begin
        the connection process.
        """
        sm.model_start_connection(dec, rule)
