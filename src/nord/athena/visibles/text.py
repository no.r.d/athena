"""
Copyright 2019 NoRD Software Foundation

This file is part of NoRD.

NoRD is free software: you can redistribute it and/or modify
it under the terms of the GNU Affero General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

NoRD is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Affero General Public License for more details.

You should have received a copy of the GNU Affero General Public License
along with NoRD.  If not, see <https://www.gnu.org/licenses/>.
"""

from nord.sigurd.utils.verboser import funlog  # noqa: F401
import nord.sigurd.managers.statemanager as sm
import nord.sigurd.handlers.keyboardhandler as keybh
from nord.sigurd.visibles.ui.text import Text as sgText
from nord.athena.visibles._base import Base as Base
from nord.sigurd.shell.model import create_connection_menu_for, cam_plane_move
from nord.sigurd.shell.modelmenu import ModelMenu, DebugMenu
from nord.sigurd.shell.menuItem import MenuItem
from nord.sigurd.utils.log import warn, debug
import nord.sigurd.managers.event as event
# import nord.athena.utils.events as ev
import json


class Text (Base, sgText):
    def __init__(self, uid, app):
        sgText.__init__(self, uid, app=app, space="athena")
        self.space = "athena"
        self._selected = False
        self._mapped_events = None
        self.onMappedFuncs.append(self.connect_to_backend)
        # Set up delegation "rules" such that the Base knows
        # what events to map to which edge rules and
        # which delegate's to look for which edge rules
        # if any delegates are in use. If there is
        # a non-delegated edge, then, all events will be
        # sent to the non-delegated edges.

        self._delegated_events = {
            'on_click': {'model': 'action', 'rule': 'flow', "s_or_t": "as_src", 'callback': self.conn_event},
            'on_value': [{'model': 'static', 'rule': 'pass', 's_or_t': 'as_src', 'callback': self.conn_event},
                         {'model': 'data', 'rule': 'assign', 's_or_t': 'as_src', 'callback': self.conn_event}]
        }

        # Must create the Model menu manually
        # since this is based off of sigurd's text
        # rather than VisNode...
        self.menu = ModelMenu()
        self.debug_menu = DebugMenu(model=self)

        @event.callback
        def callback(*args, **kwargs):
            warn("==== Calling Athena Text Selected Callback")
            sm.model_toggle_selected(self)
        self.menu.add_item(MenuItem('menudefselect', callback, title="Toggle Selected"))

        # this adds the Model generated connection menu
        self.connMenu = create_connection_menu_for(self, self.menu)
        self.menu.add_item(self.connMenu)

        # must be called after the delegates are set up so
        # that the menu can be correctly established.
        Base.__init__(self)
        # self.map_events_to_rules()

    def connect_to_backend(self):
        self.mapentry.add_chg_attr_handler('value', self.value_updated)

    def value_updated(self, attr, value):
        if attr == 'value':
            self.value = value
            self.setText(value)

    # @funlog
    def set_dimmed(self):
        """Reduce the visibility of this model."""
        if self._priorAlpha is None:
            self._priorAlpha = self.data.getColorScale()[3]
        self.data.setAlphaScale(0.2)

    # @funlog
    def set_undimmed(self):
        """Restore the visibility of this model."""
        if self._priorAlpha is not None:
            self.data.setAlphaScale(self._priorAlpha)
        self._priorAlpha = None

    @funlog
    def show_output(self, value):
        """
        Receive text to display from an external source.

        Mostly, this should come from wrigan and Assigned values.
        """
        self.value = json.loads(value)["output"]
        self.setText(self.value)

    @funlog
    def handle_clicked(self, *args, **kwargs):
        """
        Because of the event overrides in
        athena.utils.events.py handle clicked
        is only called in "sigurd" or edit mode.
        Once Athena is activated (done so via onmapstart event)
        athena's event handling takes over for sigurd's.

        As the text input is somewhat special, in that it does
        not have a model menu associated by default, one must
        be created.
        """
        sm.post_pending('model_toggle_selected', self)
        sm.data_show_menu(self)

    # @funlog
    def toggleSelected(self):  # noqa: N802
        """Toggle the selection status of this element between off and on."""
        rv = True
        if self._selected:
            self.setUnselected()
            rv = False
        else:
            self.setSelected()
        return rv

    # @funlog
    def setSelected(self):  # noqa: N802
        """Set this object as selected."""
        self._selected = True
        self._oldScale = self.data.getColorScale()
        self.data.setColorScale(255, 255, 255, 128)

    # @funlog
    def setUnselected(self):  # noqa: N802
        """Set this object as de-selected."""
        self._selected = False
        self.data.setColorScale(self._oldScale)

    @funlog
    def toggle_selected(self, *args, **kwargs):
        if self.toggleSelected():
            sm.mouse_select_object(self)
        else:
            sm.mouse_deselect_object(self)

    @funlog
    def on_click(self):
        keybh.set_target(self)
        sm.toggle_text_edit()
        self._start_val = self.contents

    @funlog
    def on_edit_done(self):
        if self.editMode:
            sm.toggle_text_edit()
            keybh.set_target(None)
        if self.contents != self._start_val:
            self.send_event('on_value', self.contents + self.tail)

    @funlog
    def handle(self, char):
        sgText.handle(self, char)
        self.send_event('on_change', self.contents + self.tail)

    @funlog
    def camPlaneMove(self, cam, mpos):
        cam_plane_move(self, cam, mpos)

    # @funlog
    def setPos(self, x, y, z):  # noqa: N802
        """Set the Panda3d NodePath's parent relative position."""
        debug("Setting position of {}: '{}' to ({:.3f}, {:.3f}, {:.3f})"
              .format(self.name, self.uid, x, y, z))
        self.data.setPos(x, y, z)
        for k in self.links:
            link = self.links[k]
            link.setPos(x, y, z)

# @funlog
    def hide(self):
        """Remove the panda3d nodepath from the render path."""
        if self.data.has_parent():
            self.data.hide()
            self.visible = False

    # @funlog
    def show(self):
        """Re-add the panda3d nodepath to the current render path."""
        self.data.show()
        self.visible = True

    @funlog
    def remove(self):
        """If also logical, request removal from map, else, just un-render"""
        if hasattr(self, 'mapentry'):
            import nord.sigurd.shell.vismap as vm
            vm.remove_node(self)
        else:
            self.unrender()

    @funlog
    def unrender(self):
        """Remove this item from the screen and logical map."""
        self.hide()
        self.data.removeNode()

        for func in self.onUnmappedFuncs:
            func()

        import nord.sigurd.managers.objectmanager as om
        om.remove(self.uid)
