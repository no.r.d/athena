"""
Copyright 2020 NoRD Software Foundation

This file is part of NoRD.

NoRD is free software: you can redistribute it and/or modify
it under the terms of the GNU Affero General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

NoRD is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Affero General Public License for more details.

You should have received a copy of the GNU Affero General Public License
along with NoRD.  If not, see <https://www.gnu.org/licenses/>.
"""

import nord.nord.Rule as Rule
from nord.athena.nodes.Location import set_position, get_position
import sys


class XXXAttach(Rule):
    """
    Since Attach is an _operate_in_reverse edge, the source rule
    must behave as the target rule instead. So, it must fetch the
    edge cargo and operate upon it as appropriate

    Descendents of this rule apply the change in location
    data stored in the edge by the visual target (effectively the source)
    and set their position by applying the difference against the visual
    source (but effective target)'s position.
    """

    def apply(self):
        """Apply the rule to this end of the edge."""
        cargo = self.edge.get_cargo()
        if cargo is None:
            return False
        npos = get_position(self.node) - self.edge.get_cargo()
        set_position(self.node, npos)
        return True


sys.modules[__name__] = XXXAttach
