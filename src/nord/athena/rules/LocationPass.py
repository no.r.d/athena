"""
Copyright 2022 NoRD Software Foundation

This file is part of NoRD.

NoRD is free software: you can redistribute it and/or modify
it under the terms of the GNU Affero General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

NoRD is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Affero General Public License for more details.

You should have received a copy of the GNU Affero General Public License
along with NoRD.  If not, see <https://www.gnu.org/licenses/>.
"""

import nord.nord.Rule as Rule
from nord.nord.exceptions import RuntimeException as RuntimeException
import sys


class LocationPass(Rule):
    """
    LocationPass is the subclass of Rule which extracts the location coordinates
    from a location node, and places them on the edge for delivery.
    """

    def apply(s):
        """
        Location Nodes place it's coordinates on the edge for delivery to the target
        """
        if hasattr(s.edge.get_source(), 'get_position'):
            s.edge.set_cargo(s.edge.get_source().get_position(s.edge.get_source()).to_dict())
            return True
        else:
            raise RuntimeException(_("Location asked to pass, "  # noqa: F821
                                     "but has nothing to pass: {}").format(s.edge.get_source()))


sys.modules[__name__] = LocationPass
