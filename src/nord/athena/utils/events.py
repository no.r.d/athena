"""
Copyright 2019 NoRD Software Foundation

This file is part of NoRD.

NoRD is free software: you can redistribute it and/or modify
it under the terms of the GNU Affero General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

NoRD is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Affero General Public License for more details.

You should have received a copy of the GNU Affero General Public License
along with NoRD.  If not, see <https://www.gnu.org/licenses/>.
"""

from nord.sigurd.managers.event import callback
from nord.sigurd.utils.verboser import funlog
import nord.sigurd.managers.filtermanager as fm
import nord.sigurd.managers.statemanager as sm
from nord.wrigan.visibles.runmenu import stop_and_remove_map
import nord.sigurd.handlers.keyboardhandler as keybh

_restore_paused = False


@callback
@funlog
def on_map_start(*args, **kwargs):
    clear_non_visibles()
    grab_state_manager()


@funlog
def grab_state_manager():
    """
    put the event state manager into
    the anthena event states so as to
    capture the user interactions.
    """
    def_ic = {
        'injects': None,
        'intercepts': {
            'mouse_freespace_click': {
                'replacement': 'noop',
                'new_intercept': 'athena_top_state'
            },
            'toggle_text_edit': {
                'new_intercept': 'athena_text_edit_mode'
            },
            'key-control-d': {
                'replacement': 'athena_map_exit'
            },
            'clickable_object_clicked': {
                'replacement': 'athena_object_clicked',
                'new_intercept': 'athena_top_state'
            },
            '__catchall__': {
                'new_intercept': 'athena_top_state',
                'replacement': 'noop'

            }
        }
    }
    sm.introduce_intercept('athena_top_state', def_ic)

    text_ic = sm.get_intercept_named('state_text_edit_mode')
    # first, re-write the new_intercepts for all the
    # sigurd event maps for the text visible
    for k in text_ic['intercepts']:
        if 'new_intercept' in text_ic['intercepts'][k]:
            if text_ic['intercepts'][k]['new_intercept'] == 'default':
                text_ic['intercepts'][k]['new_intercept'] = 'athena_top_state'
            if text_ic['intercepts'][k]['new_intercept'] == 'state_text_edit_mode':
                text_ic['intercepts'][k]['new_intercept'] = 'athena_text_edit_mode'

    # Then, make sure that a click outside of the text area
    # ends the text editing.
    text_ic['intercepts']['mouse_freespace_click'] = {
        'replacement': 'athena_end_text_edit',
        'new_intercept': 'athena_top_state'
    }

    text_ic['intercepts']['key-escape'] = {
        'replacement': 'athena_end_text_edit',
        'new_intercept': 'athena_top_state'
    }

    text_ic['intercepts']['__catchall__'] = {
        'replacement': 'athena_end_text_edit',
        'new_intercept': 'athena_top_state'
    }

    text_ic['intercepts']['data_show_menu'] = {
        'replacement': 'noop',
        'new_intercept': 'athena_text_edit_mode'
    }

    sm.introduce_intercept('athena_text_edit_mode', text_ic)
    sm.set_state_intercept('athena_top_state')


@funlog
def clear_non_visibles():
    fm.hide_not_category('execvisible')


@callback
@funlog
def on_map_stop(*args, **kwargs):
    restore_visibility()
    release_state_manager()


@funlog
def release_state_manager():
    sm.set_state_intercept('default')


@funlog
def restore_visibility():
    global _restore_paused
    if not _restore_paused:
        fm.show_not_category('execvisible')


@funlog
def pause_restore_vis():
    global _restore_paused
    _restore_paused = True


@funlog
def resume_restore_vis():
    global _restore_paused
    _restore_paused = False


@callback
@funlog
def cancel_map(*args, **kwargs):
    stop_and_remove_map()


@callback
@funlog
def clicked_on(*args, **kwargs):
    obj = args[0]
    if hasattr(obj, 'on_click'):
        obj.on_click()


@callback
@funlog
def end_text_edit(*args, **kwargs):
    obj = keybh.get_target()
    if obj and hasattr(obj, 'on_edit_done'):
        obj.on_edit_done()
