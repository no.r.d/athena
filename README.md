The NoRD Extension Athena, named after the X Windows Widget Library Athena/Xaw from the MIT X Consortium (https://en.wikipedia.org/wiki/X_Athena_Widgets).

Athena provides basic Widgets like for Buttons, Labels, Layouts, Text Inputs etc to NoRD. It facilitates the communication with the backend runtime(s) and the 
widgets as needed.

